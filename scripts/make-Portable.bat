@chcp 1251
echo off



echo.
echo ������ ���� ����������� ������ ������
echo �� ���������� ������ ���������� - ������� ������ ����: 
echo.

rem set UPX_EXECUTABLE=upx
set SEVENZIP_EXECUTABLE=7z
set PO_UTILITY=scripts\tools\poFileUtility.EXE



echo.
echo ��������� �������
echo.

set PROJNAME=uTerminal
set BUILD=Release
set LANGDIR=bin\lang
set COMMONFILES=%LANGDIR%\*.ini %LANGDIR%\%PROJNAME%.pot %LANGDIR%\%PROJNAME%.??.po %LANGDIR%\%PROJNAME%.?????.po readme.* license.* versions.* help\* bin\openssl-license.txt


echo.
echo ������� ���� html �������
echo.
call "gen-help-html.bat"

echo.
echo ������� ���� html readme
echo.
call "gen-readme-html.bat"

echo.
echo �������� SVG ����� �� light � dark
echo.
call "help-copy-light-svg.bat"

echo.
echo ������� �������� ������� ��� ������� ������:

FOR /F "delims=" %%i IN ('get-version.bat "%cd%\..\bin\uTerminal-win32-Release.exe"') DO set EXEVER=%%i
set DEST=install\v%EXEVER%
echo  - ������� %DEST%



cd ..

echo.
echo �������� ������ �������� win32
echo.
del /f /q %LANGDIR%\*win32-*.po?

echo.
echo ����������� win64 ������ �������� � ����� ��� ���� ����������
echo.
copy %LANGDIR%\%PROJNAME%-win64-%BUILD%.*.po  %LANGDIR%\%PROJNAME%.*.po

echo.
echo ����������� win64 ������� ����������� �������� � ����� ������
echo.
copy %LANGDIR%\%PROJNAME%-win64-%BUILD%.pot   %LANGDIR%\%PROJNAME%.pot

echo.
echo ������� ����� � ����� �������� ��� ����� ��������� � ���������� � .ru.po
echo.
%PO_UTILITY% %LANGDIR%\%PROJNAME%.pot %LANGDIR%\%PROJNAME%.ru.po transfer


setlocal enabledelayedexpansion
for %%a in (32,64) do (
    if %%a==32 set SPECIFIC=bin\libcrypto-1_1.dll     bin\libssl-1_1.dll
    if %%a==64 set SPECIFIC=bin\libcrypto-1_1-x64.dll bin\libssl-1_1-x64.dll

    set PROJARC=win%%a
    set BINARY=bin\*!PROJARC!-%BUILD%.exe
    set FILES=!BINARY! %COMMONFILES% !SPECIFIC!
    set FILENAME=%DEST%\%PROJNAME%-!PROJARC!-Portable.zip

    echo.
    echo ������� �����:
    echo  - ZIP: {!FILENAME!}
    echo  - Files: {!FILES!}

    del /f /q "!FILENAME!"
    
    "%UPX_EXECUTABLE%" --lzma !BINARY!
    "%SEVENZIP_EXECUTABLE%" a -tzip -mx5 !FILENAME! !FILES!
)

